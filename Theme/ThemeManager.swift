//
//  ThemeManager.swift
//  Theme
//
//  Created by Vasim Akram on 8/29/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit

@objcMembers
open class ThemeManager: NSObject {
    static public let sharedInstance = ThemeManager()
    
    open var themeColor : UIColor! = UIColor.green
    open var themeTextColor : UIColor!
    open var placeHolderHighlight: UIColor! = UIColor(red: 60/255, green: 81/255, blue: 110/255, alpha: 1)
    open var themeSecondaryColor : UIColor! = UIColor.yellow
    open var fontName : String! = ThemeManager.getSystemFontName()
    open var isRoundedButton : Bool =  true
    open var loadingImageSet:NSArray = ["1.png","2.png","3.png"]
    var loadingWindow:UIWindow = UIWindow()
    open var imageRatio : CGFloat = 2.0/1.0
   

    
    
    //Setting Theme Color with UIColor
    open func setThemeColorWithColor(_ color : UIColor) {
        self.themeColor = color
        self.setLoadingWindow()
    }
    
    //Setting Theme Secondary Color with UIColor
    open func setSecondaryThemeColorWithColor(_ color : UIColor) {
        self.themeSecondaryColor = color
        self.setLoadingWindow()
    }
    
    //Setting LoadingWindow
     func setLoadingWindow() {
        
        self.loadingWindow = UIWindow(frame: UIScreen.main.bounds)
        //let loadingView:UIView = UIView.init(frame: UIScreen.mainScreen().bounds)
        self.loadingWindow.window?.windowLevel = UIWindowLevelStatusBar
        self.loadingWindow.backgroundColor = UIColor(white: 0.0, alpha: 0.5)
        self.loadingWindow.makeKeyAndVisible()
        //self.loadingWindow.addSubview(loadingView)
        self.loadingWindow.isHidden = true
        
    }

    
    //Setting Theme Color with RGB
    open func setThemeColorWithRGB(red : Int, green : Int, blue : Int) {
        self.setLoadingWindow()
        self.themeColor = UIColor(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1.0)
    }
    
    //Setting Theme Color with Hex Value
    open func setThemeColorWithHex(_ hexValue : Int) {
        self.setLoadingWindow()
        self.setThemeColorWithRGB(red:(hexValue >> 16) & 0xff, green:(hexValue >> 8) & 0xff, blue:hexValue & 0xff)
    }
    
    
    //Setting font with font name
    open func setFontWithName(_ fontName : String) {
        self.fontName = fontName
    }
    
    //Get System Font
    static func getSystemFontName() -> String! {
        var strFontName : String = UIFont.systemFont(ofSize: 3).familyName
        strFontName = strFontName.replacingOccurrences(of: " ", with: "")
        return strFontName
    }
    
    open class func updateImageRatioConstraint(_ imgModel : UIImageView?) {
        if imgModel == nil {
            return
        }
        var oldConstraint : NSLayoutConstraint?
        for constraint : NSLayoutConstraint  in imgModel!.constraints {
            if constraint.multiplier == ThemeManager.sharedInstance.imageRatio {
                continue
            }
            else if constraint.multiplier == 2.0/1.0 {
                oldConstraint = constraint
                break
            }
        }
        if oldConstraint == nil {
            return
        }
        let newConstraint = NSLayoutConstraint(item: oldConstraint!.firstItem!, attribute: oldConstraint!.firstAttribute, relatedBy: oldConstraint!.relation, toItem: oldConstraint!.secondItem!, attribute: oldConstraint!.secondAttribute, multiplier: ThemeManager.sharedInstance.imageRatio, constant: oldConstraint!.constant)
        imgModel?.removeConstraint(oldConstraint!)
        imgModel?.addConstraint(newConstraint)
    }
    
    
}

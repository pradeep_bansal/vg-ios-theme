//
//  GSLoading.swift
//  Theme
//
//  Created by Saurabh Goyal on 9/7/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit

open class GSLoading: NSObject {
    
   static var animatedImageView:UIImageView = UIImageView()
    
    open static func addLoadingWindow ()
    {
        ThemeManager.sharedInstance.loadingWindow.isHidden = false
        
        if !self.animatedImageView.isDescendant(of: ThemeManager.sharedInstance.loadingWindow)
        {
            
            self.animatedImageView.frame=CGRect(x: 0, y: 0, width: 150, height: 40);
            self.animatedImageView.center=ThemeManager.sharedInstance.loadingWindow.center;
            
            let imagesArray:NSMutableArray = NSMutableArray()
            
            for str in ThemeManager.sharedInstance.loadingImageSet {
               let image = self.imageFromBundle(str as! String)
                if image != nil {
                    imagesArray.add(image!)
                }
            }
            
            
            if(imagesArray.count > 0)
            {
                self.animatedImageView.animationImages = NSArray.init(array: imagesArray) as? [UIImage]
                self.animatedImageView.animationDuration = 1.0;
                self.animatedImageView.animationRepeatCount = 0;
                
               ThemeManager.sharedInstance.loadingWindow.addSubview(animatedImageView)
            }
            
        }
        
        
        
        if UIApplication.shared.statusBarOrientation == UIInterfaceOrientation.landscapeRight
        {
            self.animatedImageView.transform = CGAffineTransform(rotationAngle: CGFloat(Double.pi / 2));
        }
        else
        {
            self.animatedImageView.transform = CGAffineTransform(rotationAngle: 0);
        }
        
        self.animatedImageView.startAnimating()
        
        
    }
    
    open static func removeLoadingWindow ()
    {
        self.animatedImageView.stopAnimating()
        ThemeManager.sharedInstance.loadingWindow.isHidden = true
        
    }
    
    static func imageFromBundle(_ imageName : String) -> UIImage? {
        
        return UIImage(named: imageName)
    }
    


}

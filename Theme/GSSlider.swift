//
//  GSSlider.swift
//  Theme
//
//  Created by Saurabh Goyal on 9/7/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit

@objcMembers
@IBDesignable open class GSSlider: UISlider {
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.configurePageControl()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.configurePageControl()
    }
    
    
    func configurePageControl() {
        self.thumbTintColor = ThemeManager.sharedInstance.themeColor
        self.minimumTrackTintColor = ThemeManager.sharedInstance.themeColor
    }


}

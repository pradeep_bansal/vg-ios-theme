//
//  GSButton.swift
//  Theme
//
//  Created by Vasim Akram on 8/29/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit

@objcMembers
@IBDesignable open class GSButton: UIButton {
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.configureButton()
        self.configureCornerRadius()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.configureButton()
        self.configureCornerRadius()
    }
    
    
    @objc public enum GS_buttonStyle : Int {
        case normal = 0
        case filled = 1
        case bordered = 2
    }
    
    var _buttonStyle : GS_buttonStyle = .normal
    
    var customTag : AnyObject?
    
    open var buttonStyle : GS_buttonStyle {
        get{
            return  _buttonStyle
        }
        set(buttonStyle){
            _buttonStyle = buttonStyle
            self.configureButton()
        }
    }
    
    open var fontStyle : GS_FontStyle = .regular {
        didSet{
            self.titleLabel?.font = GSFont.fontWithStyle(fontStyle, andSize: self.titleLabel!.font.pointSize)
        }
    }
    
    open var styleSheet : GS_StyleSheet = .link{
        didSet{
            self.titleLabel?.font = GSStyleSheet.fontWithStyleSheet(styleSheet)
        }
    }
    
    var _roundRectEnable : Bool = ThemeManager.sharedInstance.isRoundedButton
    @IBInspectable open var roundRectEnable : Bool {
        get{
            return _roundRectEnable
        }
        set(roundRectEnable){
            _roundRectEnable = roundRectEnable
            self.configureCornerRadius()
        }
    }
    
    func configureButton() {
        //self.titleLabel?.font = GSStyleSheet.fontWithStyleSheet(styleSheet)
        self.titleLabel?.font = GSFont.fontWithStyle(self.fontStyle, andSize: self.titleLabel!.font.pointSize)
        switch self.buttonStyle {
        case .normal:
            self.setTitleColor(ThemeManager.sharedInstance.themeColor, for: UIControlState())
            self.setTitleColor(UIColor.black.withAlphaComponent(0.38)  , for: UIControlState.disabled)
            break
        case .filled:
            self.backgroundColor = ThemeManager.sharedInstance.themeColor
            self.setTitleColor(UIColor.white, for: UIControlState())
            break
        default:
            self.setTitleColor(ThemeManager.sharedInstance.themeColor, for: UIControlState())
            self.layer.borderColor = ThemeManager.sharedInstance.themeColor.cgColor
            self.layer.borderWidth = 1.0
        }
    }
    
    func configureCornerRadius(){
        self.layer.cornerRadius = self.roundRectEnable ? 2.5 : 0.0
        self.clipsToBounds = true
    }
    
    
}

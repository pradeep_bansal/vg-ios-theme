//
//  GSStyleSheet.swift
//  Theme
//
//  Created by Vasim Akram on 12/5/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit
@objc public enum GS_StyleSheet : Int {
    case defaultStyle = 0
    case display2Black = 1
    case display2White = 2
    case display1Black = 3
    case display1White = 4
    case body4Black = 5
    case body4White = 6
    case body5Black = 7
    case body5White = 8
    case body3Black = 9
    case body3White = 10
    case quotationBlack = 11
    case quotationWhite = 12
    case headingBlack = 13
    case headingWhite = 14
    case screenTitleBlack = 15
    case screenTitleWhite = 16
    case titleBlack = 17
    case titleWhite = 18
    case primaryCTA = 19
    case subheadingBlack = 20
    case subheadingWhite = 21
    case slugTextBlack = 22
    case slugTextWhite = 23
    case body2Black = 24
    case body2White = 25
    case activeTab = 26
    case deactiveTab = 27
    case link = 28
    case body1Black = 29
    case body1White = 30
    case body6Black = 31
    case body6White = 32
    case caption1 = 33
    case caption2 = 34
    case error = 35
    case body21Black = 36
    case body21White = 37
    case display3Black = 38
    case display3White = 39
    case body7White = 40
    case body8White = 41

}

@objcMembers
open class GSStyleSheet: NSObject {
    static open func fontWithStyleSheet(_ style : GS_StyleSheet) -> UIFont {
        
        switch style {
        case .display3Black, .display3White:
            return GSFont.fontWithStyle(.regular, andSize: 34)
        case .display2Black, .display2White:
            return GSFont.fontWithStyle(.medium, andSize: 34)
        case .display1Black, .display1White:
            return GSFont.fontWithStyle(.regular, andSize: 24)
        case .body4Black, .body4White:
            return GSFont.fontWithStyle(.light, andSize: 24)
        case .body5Black, .body5White:
            return GSFont.fontWithStyle(.bold, andSize: 20)
        case .body3Black, .body3White:
            return GSFont.fontWithStyle(.regular, andSize: 20)
        case .quotationBlack, .quotationWhite:
            return GSFont.fontWithStyle(.italic, andSize: 20)
        case .headingBlack, .headingWhite, .screenTitleBlack, .screenTitleWhite:
            return GSFont.fontWithStyle(.medium, andSize: 18)
        case .titleBlack, .titleWhite, .primaryCTA:
            return GSFont.fontWithStyle(.bold, andSize: 16)
        case .subheadingBlack, .subheadingWhite:
            return GSFont.fontWithStyle(.medium, andSize: 16)
        case .slugTextBlack, .slugTextWhite, .body2Black, .body2White, .body21Black, .body21White:
            return GSFont.fontWithStyle(.regular, andSize: 16)
        case .activeTab, .deactiveTab, .link, .body7White:
            return GSFont.fontWithStyle(.medium, andSize: 14)
        case .body1Black, .body1White, .body6Black, .body6White:
            return GSFont.fontWithStyle(.regular, andSize: 14)
        case .caption1, .caption2, .error, .body8White:
            return GSFont.fontWithStyle(.regular, andSize: 12)
        default:
            return GSFont.fontWithStyle(.regular, andSize: 14)
        }
    }
    
    static public func colorWithStyleSheet(_ style : GS_StyleSheet) -> UIColor {
        switch style {
        case .titleBlack, .subheadingBlack, .activeTab, .display2Black, .display1Black, .display3Black, .body5Black, .body3Black, .quotationBlack, .screenTitleBlack, .headingBlack, .slugTextBlack, .body2Black, .body4Black, .body6Black:
            if let themeTextColor = ThemeManager.sharedInstance.themeTextColor {
                return themeTextColor
            }
            return UIColor.black.withAlphaComponent(0.87)
        case .display1White:
            return UIColor.white.withAlphaComponent(0.87)
        case .body8White:
            return UIColor.white.withAlphaComponent(0.70)
        case .slugTextWhite:
            return UIColor.white.withAlphaComponent(0.69)
        case .body1Black, .body21Black:
            return UIColor.black.withAlphaComponent(0.54)
        case .deactiveTab:
            return UIColor.black.withAlphaComponent(0.53)
        case .caption1:
            return UIColor.black.withAlphaComponent(0.37)
        case .display2White, .display3White, .body5White, .body3White, .quotationWhite, .screenTitleWhite, .headingWhite, .primaryCTA, .titleWhite, .subheadingWhite, .body2White, .body21White, .body1White, .body4White, .body6White, .body7White:
            return UIColor.white
        case .link, .caption2:
            return ThemeManager.sharedInstance.themeColor
        case .error:
            return UIColor.red
        default:
            return UIColor.black
        }
    }
    
    static open func textAlignmentWithStyleSheet(_ style : GS_StyleSheet) -> NSTextAlignment {
        switch style {
        case .activeTab, .deactiveTab, .link, .primaryCTA, .slugTextBlack, .slugTextWhite:
            return .center
        default:
            return .left
        }
    }
}

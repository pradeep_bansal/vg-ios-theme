//
//  GSActivityIndicator.swift
//  Theme
//
//  Created by Vasim Akram on 9/6/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit
@objcMembers
@IBDesignable open class GSActivityIndicator: UIActivityIndicatorView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)
        self.configureActivityIndicator()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.configureActivityIndicator()
    }
    
    @IBInspectable open var activityTintColor : UIColor = ThemeManager.sharedInstance.themeColor {
        didSet{
            self.configureActivityIndicator()
        }
    }
    
    func configureActivityIndicator() {
        self.color = self.activityTintColor
        self.tintColor = UIColor.lightGray
    }

}

//
//  GSFont.swift
//  Theme
//
//  Created by Vasim Akram on 9/21/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit

@objc public enum GS_FontStyle : Int {
    case regular = 0
    case italic = 1
    case bold = 2
    case light = 3
    case thin = 4
    case medium = 5
    case boldItalic = 6
    case lightItalic = 7
    case thinItalic = 8
    case mediumItalic = 9
}

@objcMembers
open class GSFont: UIFont {

    static open func fontWithStyle(_ style : GS_FontStyle, andSize size : CGFloat) -> UIFont {
        
        let fontStyle = getFontStyleString(style)
        let fontName = "\(ThemeManager.sharedInstance.fontName!)-\(fontStyle)"
        var objFont : UIFont? = UIFont(name: fontName, size: size)
        
        if objFont == nil {
            objFont = UIFont(name: ThemeManager.sharedInstance.fontName, size: size)
        }
        
        if objFont == nil {
            return UIFont.systemFont(ofSize: size)
        }
        else{
            return objFont!
        }
    }
    
    static func getFontStyleString(_ style : GS_FontStyle) -> String {
        switch style {
        case GS_FontStyle.italic:
            return "Italic"
        case GS_FontStyle.bold:
            return "Bold"
        case GS_FontStyle.light:
            return "Light"
        case GS_FontStyle.thin:
            return "Thin"
        case GS_FontStyle.medium:
            return "Medium"
        case GS_FontStyle.boldItalic:
            return "BoldItalic"
        case GS_FontStyle.lightItalic:
            return "LightItalic"
        case GS_FontStyle.thinItalic:
            return "ThinItalic"
        case GS_FontStyle.mediumItalic:
            return "MediumItalic"
        default:
            return "Regular"
        }
    }
    
}

//
//  GSPageControl.swift
//  Theme
//
//  Created by Vasim Akram on 8/29/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit

@objcMembers
open class GSPageControl: UIPageControl {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.configurePageControl()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.configurePageControl()
    }
    
    
    func configurePageControl() {
        self.currentPageIndicatorTintColor = ThemeManager.sharedInstance.themeColor
    }

}

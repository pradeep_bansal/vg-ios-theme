//
//  GSRangeSlider.swift
//  RangeSliderDemo
//
//  Created by Vasim Akram on 9/1/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit


@objc public protocol GSRangeSliderDelegate : NSObjectProtocol{
    func rangeSliderDidUpdateSliderLocation(_ slider : GSRangeSlider, lowerValue : Double, upperValue : Double)
}



@objcMembers
@IBDesignable open class GSRangeSlider: UIControl {

    
    var trackView: UIView!
    var rangeView: UIView!
    var leftThumbView: UIView!
    var rightThumbView: UIView!
    
    open weak var delegate: GSRangeSliderDelegate?
    // Values
    var _minimumValue: Double = 0
    @IBInspectable open var minimumValue : Double {
        get{
            return _minimumValue
        }
        set(minimumValue){
            var tempMinimumValue = minimumValue
            if tempMinimumValue >= self.maximumValue {
                tempMinimumValue = self.maximumValue - self.minimumDistance
            }
            if self.leftValue < tempMinimumValue {
                self.leftValue = tempMinimumValue
            }
            if self.rightValue < tempMinimumValue {
                self.rightValue = self.maximumValue
            }
            _minimumValue = tempMinimumValue
            self.checkMinimumDistance()
            self.setNeedsLayout()
        }
    }
    
    var _maximumValue: Double = 100
    @IBInspectable open var maximumValue:Double{
        get{
            return _maximumValue
        }
        set(maximumValue){
            var tempMaximumValue = maximumValue
            if tempMaximumValue <= self.minimumValue {
                tempMaximumValue = self.minimumValue + self.minimumDistance
            }
            if self.leftValue > tempMaximumValue {
                self.leftValue = self.minimumValue
            }
            if self.rightValue > tempMaximumValue {
                self.rightValue = tempMaximumValue
            }
            _maximumValue = tempMaximumValue
            self.checkMinimumDistance()
            self.setNeedsLayout()
        }
    }
    
    var _leftValue: Double = 0
    @IBInspectable open var leftValue:Double{
        get{
            return _leftValue
        }
        set(leftValue){
            var tempLeftValue = leftValue
            let allowedValue: Double = self.rightValue - self.minimumDistance
            if tempLeftValue > allowedValue {
                if self.pushable {
                    let rightSpace: Double = self.maximumValue - self.rightValue
                    let deltaLeft: Double = self.minimumDistance - (self.rightValue - tempLeftValue)
                    if deltaLeft > 0 && rightSpace > deltaLeft {
                        self.rightValue += deltaLeft
                    }
                    else {
                        tempLeftValue = allowedValue
                    }
                }
                else {
                    tempLeftValue = allowedValue
                }
            }
            if tempLeftValue < self.minimumValue {
                tempLeftValue = self.minimumValue
                if self.rightValue - tempLeftValue < self.minimumDistance {
                    self.rightValue = tempLeftValue + self.minimumDistance
                }
            }
            _leftValue = tempLeftValue
            self.setNeedsLayout()
        }
    }
    
    var _rightValue: Double = 100
    @IBInspectable open var  rightValue: Double {
        get{
            return _rightValue
        }
        set(rightValue){
            var tempRightValue = rightValue
            let allowedValue: Double = self.leftValue + self.minimumDistance
            if tempRightValue < allowedValue {
                if self.pushable {
                    let leftSpace: Double = self.leftValue - self.minimumValue
                    let deltaRight: Double = self.minimumDistance - (tempRightValue - self.leftValue)
                    if deltaRight > 0 && leftSpace > deltaRight {
                        self.leftValue -= deltaRight
                    }
                    else {
                        tempRightValue = allowedValue
                    }
                }
                else {
                    tempRightValue = allowedValue
                }
            }
            if tempRightValue > self.maximumValue {
                tempRightValue = self.maximumValue
                if tempRightValue - self.leftValue < self.minimumDistance {
                    self.leftValue = tempRightValue - self.minimumDistance
                }
            }
            _rightValue = tempRightValue
            self.setNeedsLayout()
        }
    }
    
    
    var _minimumDistance: Double = 1.0
    open var minimumDistance: Double {
        get{
            return _minimumDistance
        }
        set(minimunDistance){
            var tempMinimunDistance = minimunDistance
            let distance: Double = self.maximumValue - self.minimumValue
            if tempMinimunDistance > distance {
                tempMinimunDistance = distance
            }
            if self.rightValue - self.leftValue < tempMinimunDistance {
                // Reset left and right values
                self.leftValue = self.minimumValue
                self.rightValue = self.maximumValue
            }
            _minimumDistance = tempMinimunDistance
            self.setNeedsLayout()
        }
    }
    
    
    @IBInspectable open var stepValue: Double = 1
    open var pushable = false
    open var disableOverlapping = false
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    //*****************************
    //Initialization
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.setDefaults()
        self.setUpViewComponents()
        
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.setDefaults()
        self.setUpViewComponents()
        
    }
    
    
    //Public methods
    
    open func setMinValue(_ minValue: Double, maxValue: Double) {
        self.maximumValue = maxValue
        self.minimumValue = minValue
    }
    
    open func setLeftValue(_ leftValue: Double, rightValue: Double) {
        self.rightValue = rightValue
        self.leftValue = leftValue
    }
    
    // MARK: - Configuration
    
    func setDefaults() {
        self.minimumValue = 0
        self.maximumValue = 100
        self.leftValue = self.minimumValue
        self.rightValue = self.maximumValue
        self.minimumDistance = 1
        self.stepValue = 5
        self.disableOverlapping = true
    }
    
    
    func setUpViewComponents() {
        self.isMultipleTouchEnabled = true
        // Init track
        self.trackView = self.getTrackViewImage(.lightGray)
        self.addSubview(self.trackView)
        
        // Init range
        self.rangeView = self.getTrackViewImage(ThemeManager.sharedInstance.themeColor)
        self.addSubview(self.rangeView)
        
        // Init left thumb
        self.leftThumbView = self.getThumbViewImage()
        self.addSubview(self.leftThumbView)
        
        // Add left pan recognizer
        let leftPanRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleLeftPan))
        self.leftThumbView.addGestureRecognizer(leftPanRecognizer)
        
        // Init right thumb
        self.rightThumbView = self.getThumbViewImage()
        self.addSubview(self.rightThumbView)
        
        // Add right pan recognizer
        let rightPanRecognizer = UIPanGestureRecognizer(target: self, action: #selector(self.handleRightPan))
        self.rightThumbView.addGestureRecognizer(rightPanRecognizer)
        
        self.backgroundColor = UIColor.clear
    }
    
    
    // MARK: - Layout
    
    override open func layoutSubviews() {
        // Calculate coords & sizes
        let width: Double = Double(self.frame.width)
        let height: Double = Double(self.frame.height)
        let trackHeight: Double = Double(trackView.frame.size.height)
        let leftThumbImageSize = self.leftThumbView.frame.size
        let rightThumbImageSize = self.rightThumbView.frame.size
        var leftAvailableWidth: Double = width - Double(leftThumbImageSize.width)
        var rightAvailableWidth: Double = width - Double(rightThumbImageSize.width)
        if self.disableOverlapping {
            leftAvailableWidth -= Double(leftThumbImageSize.width)
            rightAvailableWidth -= Double(rightThumbImageSize.width)
        }
        let leftInset: Double = Double(leftThumbImageSize.width) / 2
        let rightInset: Double = Double(rightThumbImageSize.width) / 2
        let trackRange: Double = self.maximumValue - self.minimumValue

        var leftX: Double = floor((self.leftValue - self.minimumValue) / trackRange * leftAvailableWidth)
        if leftX.isNaN {
            leftX = 0.0
        }
        
        var rightX: Double = floor( (self.rightValue - self.minimumValue) / trackRange * rightAvailableWidth)
        if rightX.isNaN {
            rightX = 0.0
        }
        
        let trackY: Double = (height - trackHeight) / 2
        let gap: Double = 1.0
        // Set track frame
        var trackX: Double = gap
        var trackWidth: Double = width - gap * 2
        if self.disableOverlapping {
            trackX += leftInset
            trackWidth -= leftInset + rightInset
        }
        self.trackView.frame = CGRect(x: CGFloat(trackX), y: CGFloat(trackY), width: CGFloat(trackWidth), height: CGFloat(trackHeight))
        
        
        // Set range frame
        var rangeWidth: Double = rightX - leftX
        if self.disableOverlapping {
            rangeWidth += rightInset + gap
        }
        self.rangeView.frame = CGRect(x: CGFloat(leftX + leftInset), y: CGFloat(trackY), width: CGFloat(rangeWidth), height: CGFloat(trackHeight))
        // Set left & right thumb frames
        leftX += leftInset
        rightX += rightInset
        if self.disableOverlapping {
            rightX = rightX + rightInset * 2 - gap
        }
        
        self.leftThumbView.center = CGPoint(x: CGFloat(leftX), y: CGFloat(height) / 2)
        self.rightThumbView.center = CGPoint(x: CGFloat(rightX), y: CGFloat(height) / 2)
        
    }
    
    
    // MARK: - Gesture recognition
    
    @objc func handleLeftPan(_ gesture: UIPanGestureRecognizer) {
        if gesture.state == .began || gesture.state == .changed {
            //Fix when minimumDistance = 0.0 and slider is move to 1.0-1.0
            self.bringSubview(toFront: self.leftThumbView)
            let translation = gesture.translation(in: self)
            let trackRange: Double = self.maximumValue - self.minimumValue
            let width: Double = Double(self.frame.width) - Double(self.leftThumbView.frame.width)
            // Change left value
            self.leftValue += Double(translation.x) / width * trackRange
            gesture.setTranslation(CGPoint.zero, in: self)
            self.sendActions(for: .valueChanged)
        }
        if gesture.state == .ended {
            self.leftValue = self.calculateLeftValue(self.leftValue)
            if pushable && self.rightValue - self.leftValue < self.stepValue {
                self.rightValue = self.leftValue + self.stepValue
            }
            self.sendActions(for: .valueChanged)
        }
        
        if (self.delegate != nil) {
            let aLeftValue: Double = self.calculateLeftValue(self.leftValue)
            let aRightValue: Double = pushable ? self.calculateRightValue(self.rightValue) : self.rightValue
            self.delegate!.rangeSliderDidUpdateSliderLocation(self, lowerValue: aLeftValue, upperValue: aRightValue)
        }
    }
    
    
    @objc func handleRightPan(_ gesture: UIPanGestureRecognizer) {
        if gesture.state == .began || gesture.state == .changed {
            //Fix when minimumDistance = 0.0 and slider is move to 1.0-1.0
            self.bringSubview(toFront: self.rightThumbView)
            let translation = gesture.translation(in: self)
            let trackRange: Double = self.maximumValue - self.minimumValue
            let width: Double = Double(self.frame.width) - Double(self.rightThumbView.frame.width)
            // Change right value
            self.rightValue += Double(translation.x) / width * trackRange
            gesture.setTranslation(CGPoint.zero, in: self)
            self.sendActions(for: .valueChanged)
        }
        if gesture.state == .ended {
            self.rightValue = self.calculateRightValue(self.rightValue)
            if pushable && self.rightValue - self.leftValue < self.stepValue {
                self.leftValue = self.rightValue - self.stepValue
            }
            self.sendActions(for: .valueChanged)
        }
        
        if (self.delegate != nil) {
            let aLeftValue: Double = pushable ? self.calculateLeftValue(self.leftValue) : self.leftValue
            let aRightValue: Double = self.calculateRightValue(self.rightValue)
            self.delegate!.rangeSliderDidUpdateSliderLocation(self, lowerValue: aLeftValue, upperValue: aRightValue)
        }
    }
    
    
    func checkMinimumDistance() {
        if self.maximumValue - self.minimumValue < self.minimumDistance {
            self.minimumDistance = 0.0
        }
    }
    
    func calculateLeftValue(_ value: Double) -> Double {
        var aLeftValue: Double = 0.0
        if self.maximumValue - self.leftValue < self.stepValue {
            aLeftValue = self.maximumValue - self.stepValue
        }
        else if !pushable && self.rightValue - self.leftValue < self.stepValue {
            aLeftValue = self.rightValue - self.stepValue
        }
        else {
            aLeftValue = Double(roundf(Float(value / self.stepValue))) * self.stepValue
        }
        
        return aLeftValue
    }
    
    
    
    func calculateRightValue(_ value: Double) -> Double {
        var aRightValue: Double = 0.0
        if self.rightValue - self.minimumValue < self.stepValue {
            aRightValue = self.minimumValue + self.stepValue
        }
        else if !pushable && self.rightValue - self.leftValue < self.stepValue {
            aRightValue = self.leftValue + self.stepValue
        }
        else {
            aRightValue = Double(roundf(Float(value / self.stepValue))) * self.stepValue
        }
        
        return aRightValue
    }
    
    
    open func getThumbViewImage() -> UIView {
        let height: CGFloat = self.frame.height
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: height, height: height))
        myView.backgroundColor = ThemeManager.sharedInstance.themeColor
        myView.layer.borderWidth = 0.5
        myView.layer.borderColor = UIColor.gray.cgColor
        myView.layer.cornerRadius = myView.frame.size.height / 2.0
        myView.clipsToBounds = true
        return myView
    }
    
    open func getTrackViewImage(_ color : UIColor) -> UIView {
        let myView = UIView(frame: CGRect(x: 0, y: 0, width: 6, height: 4))
        myView.backgroundColor = color
        myView.layer.cornerRadius = myView.frame.size.height / 2.0
        myView.clipsToBounds = true
        
        return myView;
    }
    
    

}


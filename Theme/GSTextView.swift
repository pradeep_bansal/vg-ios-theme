//
//  GSTextView.swift
//  Theme
//
//  Created by Vasim Akram on 8/31/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit

@IBDesignable open class GSTextView: UITextView {

    
    open var fontStyle : GS_FontStyle = .regular {
        didSet{
            self.font = GSFont.fontWithStyle(fontStyle, andSize: self.font!.pointSize)
        }
    }

    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.configureTextView()
    }
    
    override public init(frame: CGRect, textContainer: NSTextContainer?) {
        super.init(frame: frame, textContainer: textContainer)
        self.configureTextView()
    }
    
    var _enableBorder : Bool = true
    @IBInspectable open var enableBorder : Bool {
        get{
            return _enableBorder
        }
        set(enableBorder){
            _enableBorder = enableBorder
            self.configureBorder()
        }
    }
    
    func configureBorder(){
        if self.enableBorder {
            self.layer.borderColor = UIColor.lightGray.cgColor
            self.layer.borderWidth = 0.5
        }
        else{
            self.layer.borderColor = UIColor.clear.cgColor
            self.layer.borderWidth = 0.0
        }
    }
    
    var _roundRectEnable : Bool = ThemeManager.sharedInstance.isRoundedButton
    @IBInspectable var roundRectEnable : Bool {
        get{
            return _roundRectEnable
        }
        set(roundRectEnable){
            _roundRectEnable = roundRectEnable
            self.configureCornerRadius()
        }
    }
    
    func configureCornerRadius(){
        self.layer.cornerRadius = self.roundRectEnable ? 5.0 : 0.0
        self.clipsToBounds = true
    }
    
    
    func configureTextView() {
        self.font = GSFont.fontWithStyle(self.fontStyle, andSize: self.font!.pointSize)
    }

}

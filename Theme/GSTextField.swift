//
//  GSTextField.swift
//  GSTextField
//
//  Created by Vasim Akram on 9/2/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit

@objc public protocol GSTextFieldDelegate : NSObjectProtocol {
    
    @objc optional func textFieldShouldBeginEditing(_ textField: GSTextField) -> Bool
    
    @objc optional func textFieldDidBeginEditing(_ textField: GSTextField)
    
    @objc optional func textFieldShouldEndEditing(_ textField: GSTextField) -> Bool
    
    @objc optional func textFieldDidEndEditing(_ textField: GSTextField)
    
    @objc optional func textField(_ textField: GSTextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool
    
    @objc optional func textFieldShouldClear(_ textField: GSTextField) -> Bool
    
    @objc optional func textFieldShouldReturn(_ textField: GSTextField) -> Bool
    
    @objc optional func textField(_ textField: GSTextField, didSelectItemAtIndex index : NSInteger)
}

@objc public protocol GSTextFieldDataSource : NSObjectProtocol{
    func noOfRowsInDropdownTextField(_ textfield : GSTextField) -> NSInteger
    func textfield(_ textfield : GSTextField, dropdownTitleForRowAtIndex index: NSInteger) -> String
    @objc optional func textfield(_ textfield : GSTextField, dropdownSubTitleForRowAtIndex index: NSInteger) -> String
}

@objc public enum GSTextFieldValidationType : Int {
    case none = 0
    case email = 1
    case phoneNumber = 2
}

@objc public enum GSTextFieldType : Int {
    case `default` = 0
    case selection = 1
    case dropDown = 2
}

@objcMembers
@IBDesignable
open class GSTextField: UIView, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var title : UILabel!
    open var textField : UITextField!
    var line : UIView!
    
    open var isInputValid : Bool = true
    open weak var delegate: GSTextFieldDelegate?
    
    var _validationType :GSTextFieldValidationType = GSTextFieldValidationType.none
    open var validationType : GSTextFieldValidationType {
        get {
            return _validationType
        }
        set(validationType){
            if validationType == .email {
                self.textField.keyboardType = UIKeyboardType.emailAddress
                self.textField.autocapitalizationType = .none
                self.textField.autocorrectionType = .no
            }
            else if validationType == .phoneNumber {
                self.textField.keyboardType = UIKeyboardType.phonePad
            }
            _validationType = validationType
        }
    }
    open var validationRegex : String?
    
    var hilightColor : UIColor = ThemeManager.sharedInstance.placeHolderHighlight
    var errorColor : UIColor = UIColor.red
    var placeHolderColor : UIColor = UIColor.lightGray
    
    var _placeHolder : String = "GS TextField"
    @IBInspectable open var placeHolder : String {
        get{
            return _placeHolder
        }
        set(placeHolder){
            _placeHolder = placeHolder
            self.title.text = placeHolder
        }
    }
    
    open var searchFromBeginning : Bool = true
    
    var _secureTextEntry : Bool = false
    @IBInspectable open var secureTextEntry : Bool{
        get{
            return _secureTextEntry
        }
        set(secureTextEntry){
            _secureTextEntry = secureTextEntry
            self.textField.isSecureTextEntry = secureTextEntry
        }
    }
    
    open var text : String {
        get{
            return textField.text!
        }
        set(newText){
            textField.text = newText
            if newText == "" && !dropDownAdded{
                moveTitleDownWithAnimation()
            }
            else{
                moveTitleUpWithAnimation()
            }
        }
    }
    
    open var dropDownArray : Array<String>! = Array()
    let tblDropDown : UITableView = UITableView()
    let aButton = UIButton()
    var dropDownAdded : Bool = false
    var filteredDropDownArray : Array<String>!
    
    open var subDetailsArray : Array<String>! = Array()
    
    open var textFieldType : GSTextFieldType = .default{
        didSet{
            self.setNeedsLayout()
            if textFieldType == .dropDown{
                self.textField.autocapitalizationType = .none
                self.textField.autocorrectionType = .no
            }
            
        }
    }
    
    open var fontStyle : GS_FontStyle = .regular {
        didSet{
            self.title.font = GSFont.fontWithStyle(fontStyle, andSize: self.title.font.pointSize)
            self.textField.font = GSFont.fontWithStyle(fontStyle, andSize: self.textField.font!.pointSize)
        }
    }
    
    @IBInspectable open var titleFontSize : CGFloat = 14 {
        didSet{
            updateTitleFontSize()
        }
    }
    @IBInspectable open var textFontSize : CGFloat = 17 {
        didSet{
            self.textField.font = GSFont.fontWithStyle(fontStyle, andSize: textFontSize)
            updateTitleFontSize()
        }
    }
    
    func updateTitleFontSize(){
        if self.title.frame.origin.y == 0 {
            title.font = GSFont.fontWithStyle(self.fontStyle, andSize: titleFontSize)
        }
        else{
            title.font = GSFont.fontWithStyle(self.fontStyle, andSize: textFontSize)
        }
    }
    
    open var visibleRowCount : NSInteger = 5
    open var dropDownRowHeight : CGFloat = 45
    
    weak open var dataSource: GSTextFieldDataSource? {
        didSet{
            reloadData()
        }
    }
    
    open var hideBottomLine : Bool = false{
        didSet{
            self.line.isHidden = hideBottomLine
        }
    }
    
    open var selectedIndex : Int? {
        set{
            self.selectedIndex = newValue
        }
        get{
            return self.dropDownArray.index(of: self.textField.text!)!
        }
    }
    
    open var minimumCharacterForSearch : NSInteger = 0
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.configureTextField()
    }
    
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.configureTextField()
    }
    
    //let symbol : UILabel = UILabel()
    let downArrow : UIImageView = UIImageView(image: UIImage(named: "down_arrow", in: Bundle(for: GSTextField.self) , compatibleWith: nil))
    
    open func configureTextField() {
        let width : CGFloat  = self.frame.width
        let height : CGFloat  = self.frame.height
        
        title = UILabel(frame: CGRect(x: 0,y: 16,width: width,height: 30))
        title.textColor = placeHolderColor
        title.font = GSFont.fontWithStyle(self.fontStyle, andSize: textFontSize)
        title.text = placeHolder
        
        self.addSubview(title)
        
        textField = UITextField(frame: CGRect(x: 0,y: 16,width: width,height: 30))
        textField.tintColor = hilightColor
        textField.font = GSFont.fontWithStyle(self.fontStyle, andSize:textField.font!.pointSize)
        textField.delegate = self
        textField.addTarget(self, action: #selector(GSTextField.textDidChange(_:)), for: UIControlEvents.editingChanged)
        self.addSubview(textField)
        
        line = UIView(frame: CGRect(x: 0,y: height - 2,width: width,height: 1))
        line.backgroundColor = placeHolderColor
        self.addSubview(line)
        
        self.backgroundColor = UIColor.clear
        
        downArrow.frame = CGRect(x: 0, y: 0, width: 18, height: 22.5)
        
        textField.rightView = downArrow
    }
    var isShown : Bool = false
    override open func layoutSubviews(){
        if isShown == false {
            let width : CGFloat  = self.frame.width
            let height : CGFloat  = self.frame.height
            
            self.textField.frame = CGRect(x: 0,y: 16,width: width,height: 30)
            self.line.frame = CGRect(x: 0,y: height - 2,width: width,height: 1)
            
            if textField.text == "" {
                self.title.frame = CGRect(x: 0,y: 16,width: width,height: 30)
                self.title.font = GSFont.fontWithStyle(self.fontStyle, andSize: textFontSize)
                self.title.textColor = self.placeHolderColor
            }
            else{
                self.title.frame = CGRect(x: 0,y: 0,width: width,height: 15)
                self.title.font = GSFont.fontWithStyle(self.fontStyle, andSize: titleFontSize)
                self.title.textColor = self.hilightColor
            }
            
            isShown = true
        }
        
        if self.textFieldType == .selection {
            textField.rightViewMode = .always
            title.isHidden = true
        }
        else{
            textField.rightViewMode = .never
            title.isHidden = false
        }
        
        line.isHidden = hideBottomLine
        
    }
    
    //MARK - UITextField Delegates
    open func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        var shouldBeginEditing = true
        if delegate != nil && delegate?.responds(to: #selector(GSTextFieldDelegate.textFieldShouldBeginEditing(_:))) == true {
            shouldBeginEditing = (delegate?.textFieldShouldBeginEditing!(self))!
        }
        
        if textFieldType == .selection {
            self.filteredDropDownArray = self.dropDownArray
            if filteredDropDownArray.count > 0 {
                self.addDropDown()
            }
            return false;
        }
        
        if shouldBeginEditing == true {
            self.moveTitleUpWithAnimation()
        }
        
        return shouldBeginEditing
    }
    
    open func textFieldDidBeginEditing(_ textField: UITextField) {
        if delegate != nil && delegate?.responds(to: #selector(GSTextFieldDelegate.textFieldDidBeginEditing(_:))) == true {
            (delegate?.textFieldDidBeginEditing!(self))!
        }
        let width : CGFloat  = self.frame.width
        let height : CGFloat  = self.frame.height
        self.line.frame = CGRect(x: 0,y: height - 2,width: width,height: 2)
        self.line.backgroundColor = self.hilightColor
    }
    
    open func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        var shouldEndEditing = true
        if delegate != nil && delegate?.responds(to: #selector(GSTextFieldDelegate.textFieldShouldEndEditing(_:))) == true {
            shouldEndEditing = (delegate?.textFieldShouldEndEditing!(self))!
        }
        return shouldEndEditing
    }
    
    open func textFieldDidEndEditing(_ textField: UITextField) {
        self.moveTitleDownWithAnimation()
        
        if delegate != nil && delegate?.responds(to: #selector(GSTextFieldDelegate.textFieldDidEndEditing(_:))) == true {
            (delegate?.textFieldDidEndEditing!(self))!
        }
        
        if self.textFieldType == .dropDown {
            self.removeDropDown(nil)
        }
        
        let width : CGFloat  = self.frame.width
        let height : CGFloat  = self.frame.height
        self.line.frame = CGRect(x: 0,y: height - 2,width: width,height: 1)
        self.line.backgroundColor = self.isInputValid ? self.placeHolderColor : self.errorColor
    }
    
    open func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        var shouldChangeg = true
        if delegate != nil && delegate?.responds(to: #selector(GSTextFieldDelegate.textField(_:shouldChangeCharactersInRange:replacementString:))) == true {
            shouldChangeg = (delegate?.textField!(self, shouldChangeCharactersInRange: range, replacementString: string))!
        }
        return shouldChangeg
    }
    
    
    open func textFieldShouldClear(_ textField: UITextField) -> Bool {
        var shouldClear = true
        if delegate != nil && delegate?.responds(to: #selector(GSTextFieldDelegate.textFieldShouldClear(_:))) == true {
            shouldClear = (delegate?.textFieldShouldClear!(self))!
        }
        return shouldClear
    }
    
    open func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        var shouldReturn = true
        if delegate != nil && delegate?.responds(to: #selector(GSTextFieldDelegate.textFieldShouldReturn(_:))) == true {
            shouldReturn = (delegate?.textFieldShouldReturn!(self))!
        }
        return shouldReturn
    }
    
    @objc open func textDidChange(_ textInput: UITextField){
        if self.textFieldType == .dropDown {
            if dropDownAdded == true {
                self.text = ddInputField.text!
            }
            if (textInput.text?.count)! >= self.minimumCharacterForSearch {
                self.filteredDropDownArray = dropDownArray.filter({ (string) -> Bool in
                    if searchFromBeginning == true {
                        return string.lowercased().hasPrefix(textInput.text!.lowercased())
                    }
                    else{
                        return string.lowercased().contains(textInput.text!.lowercased())
                    }
                })
                
                if self.filteredDropDownArray?.count == 0 {
                    removeDropDown(nil)
                }
                else if dropDownAdded == false {
                    addDropDown()
                    dropDownAdded = true
                }
                else{
                    updateDropDownFrame()
                }
                tblDropDown.reloadData()
            }
            else if dropDownAdded == true {
                removeDropDown(nil)
            }
        }
        checkForValidation()
    }
    
    
    func moveTitleUpWithAnimation(){
        let width : CGFloat  = self.frame.width
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
            self.title.frame = CGRect(x: 0,y: 0,width: width,height: 15)
            self.title.font = GSFont.fontWithStyle(self.fontStyle, andSize:self.titleFontSize)
            self.title.textColor = self.hilightColor
        }, completion: nil)
    }
    
    func moveTitleDownWithAnimation(){
        let width : CGFloat  = self.frame.width
        UIView.animate(withDuration: 0.2, delay: 0, options: UIViewAnimationOptions.curveLinear, animations: {
            if self.textField.text == "" {
                self.title.frame = CGRect(x: 0,y: 16,width: width,height: 30)
                self.title.font = GSFont.fontWithStyle(self.fontStyle, andSize: self.textFontSize)
                self.title.textColor = self.placeHolderColor
            }
        }, completion: nil)
    }
    
    func checkForValidation(){
        if textField.text == "" {
            self.isInputValid = true
        }
        else if self.validationType == .email {
            let emailRegex = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
            let emailTest = NSPredicate(format:"SELF MATCHES %@" , emailRegex)
            self.isInputValid = emailTest.evaluate(with: textField.text)
        }
        else if self.validationRegex != nil {
            let emailTest = NSPredicate(format:"SELF MATCHES %@" , validationRegex!)
            self.isInputValid = emailTest.evaluate(with: textField.text)
        }
        else if self.validationType == .phoneNumber {
            let numRegex = "[0-9.+-]+";
            let numTest = NSPredicate(format:"SELF MATCHES %@" , numRegex)
            self.isInputValid =  numTest.evaluate(with: textField.text)
        }
        
        if self.isInputValid == true {
            self.line.backgroundColor = self.hilightColor
        }
        else{
            self.line.backgroundColor = self.errorColor
        }
    }
    
    
    //MARK : - DropDown custom methods
    
    @objc func removeDropDown(_ sender : AnyObject?) {
        var frame : CGRect = self.tblDropDown.frame
        UIView.animate(withDuration: 0.2, animations: {
            frame.size.height = 0
            self.tblDropDown.frame = frame
        }, completion: { (finished) in
            self.aButton.removeFromSuperview()
            self.loadingWindow.isHidden = true
            self.dropDownAdded = false
        }) 
        if  self.dropDownAdded == true && self.textFieldType == .dropDown {
            self.text = self.ddInputField.text!
        }
        if sender != nil {
            self.endEditing(true)
            self.moveTitleDownWithAnimation()
        }
    }
    
    func updateDropDownFrame(){
        var frame : CGRect = self.tblDropDown.frame
        UIView.animate(withDuration: 0.2, animations: {
            frame.size.height = self.getDropDownTableHeight()
            self.tblDropDown.frame = frame
        }, completion: { (finished) in
            
        }) 
    }
    var loadingWindow : UIWindow = UIWindow(frame: UIScreen.main.bounds)
    
    var ddInputField : UITextField = UITextField()
    
    func addDropDown(){
        self.textField.bringSubview(toFront: self.loadingWindow)
        self.loadingWindow.window?.windowLevel = UIWindowLevelStatusBar
        self.loadingWindow.backgroundColor = UIColor(white: 0.0, alpha: 0.2)
        self.loadingWindow.makeKeyAndVisible()
        self.loadingWindow.isHidden = false
        
        let textFieldRect = self.convert(self.bounds, to: nil)
        
        if self.textFieldType == .dropDown {
            ddInputField.frame = self.convert(self.textField.frame, to: nil)
            ddInputField.autocorrectionType = .no
            ddInputField.autocapitalizationType = .none
            ddInputField.spellCheckingType = .no
            ddInputField.font = textField.font
            ddInputField.text = self.textField.text!
            ddInputField.textColor = UIColor.clear
            ddInputField.becomeFirstResponder()
            ddInputField.addTarget(self, action: #selector(GSTextField.textDidChange(_:)), for: UIControlEvents.editingChanged)
            aButton.addSubview(ddInputField)
        }
        
        aButton.frame = UIScreen.main.bounds
        aButton.addTarget(self, action: #selector(GSTextField.removeDropDown), for: UIControlEvents.touchUpInside)
        
        self.loadingWindow.addSubview(aButton)
        
        var frame = CGRect(x: textFieldRect.origin.x, y: textFieldRect.origin.y + textFieldRect.size.height, width: textFieldRect.size.width, height: 0)
        
        let screenHeight = self.loadingWindow.frame.size.height
        let tableHeight = self.getDropDownTableHeight()
        
        if (self.textFieldType == .selection && (screenHeight - frame.origin.y - tableHeight) < 0) {
            var yPos = screenHeight - tableHeight
            yPos = yPos / 2.0
            frame.origin.y = yPos
            var xPos = self.loadingWindow.frame.size.width - frame.size.width
            xPos = xPos / 2.0
            frame.origin.x = xPos
        }
        
        self.tblDropDown.frame = frame
        
        UIView.animate(withDuration: 0.3, animations: {
            frame.size.height = self.getDropDownTableHeight()
            self.tblDropDown.frame = frame
        }, completion: nil)
        
        tblDropDown.layer.cornerRadius = 5.0
        tblDropDown.delegate = self
        tblDropDown.dataSource = self
        
        tblDropDown.backgroundColor = UIColor(red: 247/255.0, green: 247/255.0, blue: 247/255.0, alpha: 1)
        tblDropDown.reloadData()
        
        aButton.addSubview(tblDropDown)
    }
    
    func getDropDownTableHeight() -> CGFloat{
        return dropDownRowHeight * CGFloat(min(visibleRowCount, self.filteredDropDownArray.count))
    }
    
    
    open func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return self.filteredDropDownArray.count
    }
    
    open func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return dropDownRowHeight
    }
    
    open func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell : UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "")
        if cell == nil {
            if self.subDetailsArray.count > 0 {
                cell = UITableViewCell(style: UITableViewCellStyle.subtitle, reuseIdentifier: "")
                cell!.detailTextLabel?.text = subDetailsArray[indexPath.row]
                cell?.detailTextLabel?.textColor = UIColor.black.withAlphaComponent(0.37)
                cell?.detailTextLabel?.font = GSFont.fontWithStyle(.regular, andSize: (cell?.detailTextLabel?.font.pointSize)!)
            }
            else {
                cell = UITableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "")
                cell?.textLabel?.font = GSFont.fontWithStyle(.regular, andSize: (cell?.textLabel?.font.pointSize)!)
            }
            cell?.backgroundColor = UIColor.clear
        }
        cell!.textLabel?.text = filteredDropDownArray[indexPath.row]
        if self.subDetailsArray.count > 0 {
            cell!.detailTextLabel?.text = subDetailsArray[indexPath.row]
        }
        
        return cell!
    }
    
    
    open func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let cell : UITableViewCell = tableView.cellForRow(at: indexPath)!
        if self.textFieldType == .dropDown {
            ddInputField.text = cell.textLabel?.text
        }
        else{
            textField.text = cell.textLabel?.text
        }
        removeDropDown(cell)
        
        if delegate != nil && delegate?.responds(to: #selector(GSTextFieldDelegate.textField(_:didSelectItemAtIndex:))) == true {
            delegate?.textField!(self, didSelectItemAtIndex: self.selectedIndex!)
        }
    }
    
    open func reloadData(){
        dropDownArray.removeAll()
        subDetailsArray.removeAll()
        var noOfRows : NSInteger = 0
        if dataSource != nil && dataSource?.responds(to: #selector(GSTextFieldDataSource.noOfRowsInDropdownTextField(_:))) == true {
            noOfRows = (dataSource?.noOfRowsInDropdownTextField(self))!
        }
        
        if dataSource != nil && dataSource?.responds(to: #selector(GSTextFieldDataSource.textfield(_:dropdownTitleForRowAtIndex:))) == true {
            for index : NSInteger in 0 ..< noOfRows{
                let title = dataSource?.textfield(self, dropdownTitleForRowAtIndex: index)
                dropDownArray.append(title!)
            }
            if self.textFieldType == .selection && self.textField.text == "" {
                self.textField.text = dropDownArray.first
            }
        }
        
        if dataSource != nil && dataSource?.responds(to: #selector(GSTextFieldDataSource.textfield(_:dropdownSubTitleForRowAtIndex:))) == true {
            for index : NSInteger in 0 ..< noOfRows{
                let subTitle = dataSource?.textfield!(self, dropdownSubTitleForRowAtIndex: index)
                subDetailsArray.append(subTitle!)
            }
        }
    }
    
}

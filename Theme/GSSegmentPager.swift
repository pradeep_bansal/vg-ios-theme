//
//  GSSegmentPager.swift
//  Theme
//
//  Created by Vasim Akram on 9/7/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit

@objc public protocol GSSegmentPagerDelegate : NSObjectProtocol{
    optional func segmentPager(segmentPager : GSSegmentPager, didMoveFromIndex fromIndex: NSInteger, toIndex : NSInteger)
}

@objc public protocol GSSegmentPagerDataSource : NSObjectProtocol{
    func noOfSegmentInSegmentPager(segmentPager : GSSegmentPager) -> NSInteger
    func segmentPager(segmentPager : GSSegmentPager, titleForSegmentAtIndex index: NSInteger) -> String
    func segmentPager(segmentPager : GSSegmentPager, viewControllerForSegmentAtIndex index: NSInteger) -> UIViewController?
}


@objc public enum SegmentTitleWidthStyle : Int {
    case Fixed = 0
    case Dynamic = 1
}

public class GSSegmentPager: UIView, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    let menuScrollView : UIScrollView = UIScrollView()
    let selectedSegmentIndiciter : UIView = UIView()
    let segmentTitleView : UIView = UIView()
    
    let pageViewController : UIPageViewController = UIPageViewController(transitionStyle: UIPageViewControllerTransitionStyle.Scroll, navigationOrientation: UIPageViewControllerNavigationOrientation.Horizontal, options: nil)
    
    //var viewControllers : Array<UIViewController> = Array()
    
    public var segmentHeight : CGFloat = 40.0{
        didSet{
            self.setNeedsDisplay()
        }
    }
    public var selectedSegmentIndiciterHeight : CGFloat = 3.0{
        didSet{
            self.setNeedsDisplay()
        }
    }
    
    
    weak public var delegate: GSSegmentPagerDelegate?
    weak public var dataSource: GSSegmentPagerDataSource? {
        didSet{
            reloadData()
        }
    }
    
    public var minimumSegmentWidth : CGFloat = 0 {
        didSet{
            self.setNeedsDisplay()
        }
    }
    public var segmentTitleWidthStyle : SegmentTitleWidthStyle = .Dynamic {
        didSet{
            self.setNeedsDisplay()
        }
    }
    
    var contentWidth : CGFloat = 0
    
    public var segmentTitleFont : UIFont = GSFont.fontWithStyle(.Regular, andSize: 15){
        didSet{
            self.setNeedsDisplay()
        }
    }
    
    var segmentTitleArray : Array<UIButton> = Array()
    
    public var currentSegmentIndex : NSInteger = 0
    
    @IBInspectable public var segmentTitleBackgroundColor : UIColor = UIColor.whiteColor(){
        didSet{
            self.segmentTitleView.backgroundColor = segmentTitleBackgroundColor
        }
    }
    
    @IBInspectable public var unselectedTitleColor : UIColor = UIColor.grayColor(){
        didSet{
            for button : UIButton in self.segmentTitleArray{
                button.setTitleColor(self.unselectedTitleColor, forState: UIControlState.Normal)
            }
        }
    }
    
    @IBInspectable public var selectedTitleColor : UIColor = UIColor.blackColor(){
        didSet{
            for button : UIButton in self.segmentTitleArray{
                button.setTitleColor(self.selectedTitleColor, forState: UIControlState.Normal)
            }
        }
    }
    
    public var dropSegmentTitleShadow : Bool = true {
        didSet{
            self.segmentTitleView.layer.shadowColor = dropSegmentTitleShadow ? UIColor.blackColor().CGColor : UIColor.clearColor().CGColor
        }
    }
    
    /*
     // Only override drawRect: if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func drawRect(rect: CGRect) {
     // Drawing code
     }
     */
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.configureSegmentPager()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.configureSegmentPager()
    }
    
    override public func layoutSubviews() {
        let menuScrollViewHeight = segmentHeight + selectedSegmentIndiciterHeight
        self.menuScrollView.frame = CGRectMake(0, 0, self.frame.size.width, menuScrollViewHeight)
        self.pageViewController.view.frame = CGRectMake(0, menuScrollViewHeight, self.frame.size.width, self.frame.size.height - menuScrollViewHeight)
        self.segmentTitleView.frame = self.menuScrollView.frame
        
        self.contentWidth = 0
        var maxTitleWidth : CGFloat = 0
        for button : UIButton in self.segmentTitleArray{
            let title : String = (button.titleLabel?.text)!
            let titleWidth : CGFloat = getWidthForString(title)
            let finalTitleWidth = max(self.minimumSegmentWidth, titleWidth)
            maxTitleWidth = max(maxTitleWidth, finalTitleWidth)
            self.contentWidth +=  finalTitleWidth
        }
        
        if self.segmentTitleWidthStyle == .Fixed {
            self.contentWidth = CGFloat(self.segmentTitleArray.count) * maxTitleWidth
        }
        
        var padding : CGFloat = 0;
        if(self.contentWidth < self.frame.size.width){
            padding = (self.frame.size.width - self.contentWidth) / 2
        }
        var lastButtonX = padding
        for index : NSInteger in 0 ..< self.segmentTitleArray.count {
            let button : UIButton = self.segmentTitleArray[index]
            if self.segmentTitleWidthStyle == .Fixed {
                button.frame = CGRectMake(lastButtonX, 0, maxTitleWidth, self.segmentHeight)
                lastButtonX += maxTitleWidth
            }
            else{
                let title : String = (button.titleLabel?.text)!
                let titleWidth : CGFloat = getWidthForString(title)
                let buttonWidth = max(self.minimumSegmentWidth, titleWidth)
                
                button.frame = CGRectMake(lastButtonX, 0, buttonWidth, self.segmentHeight)
                lastButtonX += buttonWidth
            }
        }
        self.menuScrollView.contentSize = CGSizeMake(self.contentWidth, menuScrollViewHeight)
        
        
        if self.segmentTitleArray.count - 1 >= currentSegmentIndex {
            let button : UIButton = self.segmentTitleArray[currentSegmentIndex]
            button.selected = true
            
            self.selectedSegmentIndiciter.frame = CGRectMake(button.frame.origin.x, segmentHeight, button.frame.size.width, selectedSegmentIndiciterHeight)
            
        }
    }
    
    public func configureSegmentPager() {
        
        self.addSubview(self.pageViewController.view)
        
        self.addSubview(self.segmentTitleView)
        self.segmentTitleView.backgroundColor = self.segmentTitleBackgroundColor
        
        self.menuScrollView.showsHorizontalScrollIndicator = false
        self.selectedSegmentIndiciter.backgroundColor = ThemeManager.sharedInstance.themeColor
        self.menuScrollView.addSubview(self.selectedSegmentIndiciter)
        self.segmentTitleView.addSubview(self.menuScrollView)
        self.pageViewController.delegate = self
        self.pageViewController.dataSource = self
        
        
        self.segmentTitleView.layer.shadowColor = self.dropSegmentTitleShadow ? UIColor.blackColor().CGColor : UIColor.clearColor().CGColor
        self.segmentTitleView.layer.shadowOpacity = 0.3
        self.segmentTitleView.layer.shadowRadius = 2.0
        self.segmentTitleView.layer.shadowOffset = CGSizeMake(0, 2.0)
    }
    
    func getSegmentButtonForIndex(index : NSInteger, withTitle title:String) -> UIButton{
        let button : UIButton = UIButton()
        button.setTitle(title, forState: UIControlState.Normal)
        button.setTitleColor(self.unselectedTitleColor, forState: UIControlState.Normal)
        button.setTitleColor(self.selectedTitleColor, forState: UIControlState.Selected)
        button.titleLabel?.font = segmentTitleFont
        button.tag = index
        button.addTarget(self, action: #selector(GSSegmentPager.segmentButtonAction(_:)), forControlEvents: UIControlEvents.TouchUpInside)
        
        return button
    }
    
    
    public func reloadData(){
        
        for viewObj in self.menuScrollView.subviews{
            if viewObj.isKindOfClass(UIButton.self){
                viewObj.removeFromSuperview()
            }
        }
        self.segmentTitleArray.removeAll()
        
        
        if dataSource != nil && dataSource?.respondsToSelector(#selector(GSSegmentPagerDataSource.noOfSegmentInSegmentPager(_:))) == true {
            let noOfSegments : NSInteger = dataSource!.noOfSegmentInSegmentPager(self)
            
            for index : NSInteger in 0 ..< noOfSegments{
                let title : String = dataSource!.segmentPager(self, titleForSegmentAtIndex: index)
                let button : UIButton = self.getSegmentButtonForIndex(index, withTitle: title)
                self.menuScrollView.addSubview(button)
                self.segmentTitleArray.append(button)
            }
            
            //            for index : NSInteger in 0 ..< noOfSegments{
            //                let viewController : UIViewController = dataSource!.segmentPager(self, viewControllerForSegmentAtIndex: index)
            //                self.viewControllers.append(viewController)
            //            }
            
            if currentSegmentIndex >= self.segmentTitleArray.count {
                self.currentSegmentIndex = 0
                self.menuScrollView.contentOffset = CGPointZero
            }
            
            
            if self.segmentTitleArray.count > 0 {
                let viewController = self.getViewControllerAtIndex(self.currentSegmentIndex)
                self.pageViewController.setViewControllers([viewController!], direction: UIPageViewControllerNavigationDirection.Forward, animated: false, completion: nil)
            }
            
            self.setNeedsLayout()
        }
        
        if let parentViewController : UIViewController = dataSource as? UIViewController {
            parentViewController.automaticallyAdjustsScrollViewInsets = false
        }
    }
    
    //MARK - UIPageViewController DataSource
    public func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController?{
        var index : NSInteger = currentSegmentIndex
        if index >= self.segmentTitleArray.count || index == 0 {
            return nil
        }
        index -= 1
        return self.getViewControllerAtIndex(index)
    }
    
    public func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController?{
        var index : NSInteger = currentSegmentIndex
        if index < 0 || index == self.segmentTitleArray.count - 1 {
            return nil
        }
        index += 1
        return self.getViewControllerAtIndex(index)
    }
    
    public func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let fromIndex = previousViewControllers.last!.view.tag
        let toIndex = pageViewController.viewControllers!.last!.view.tag
        currentSegmentIndex = toIndex
        self.moveHighlitedSegmentBarFromIndex(fromIndex, toIndex: toIndex)
    }
    
    //MARK - some custom methods
    func getViewControllerAtIndex(index : NSInteger) -> UIViewController? {
        if self.segmentTitleArray.count == 0 || index < 0 || index >= self.segmentTitleArray.count  {
            return nil
        }
        else {
            let viewController : UIViewController? = dataSource!.segmentPager(self, viewControllerForSegmentAtIndex: index)
            if viewController != nil {
                viewController!.view.tag = index
            }
            return viewController
        }
    }
    
    
    func moveHighlitedSegmentBarFromIndex(fromIndex : NSInteger, toIndex : NSInteger){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),{
            var buttonPreX : CGFloat = -1
            var buttonPreWidth : CGFloat = 0
            if toIndex - 1 >= 0 {
                buttonPreWidth = self.segmentTitleArray[toIndex - 1].frame.size.width
                buttonPreX = self.segmentTitleArray[toIndex - 1].frame.origin.x
            }
            
            let buttonCurWidth = self.segmentTitleArray[toIndex].frame.size.width
            let buttonCurX = self.segmentTitleArray[toIndex].frame.origin.x
            
            var buttonNexWidth : CGFloat = 0
            if toIndex + 1 < self.segmentTitleArray.count {
                buttonNexWidth = self.segmentTitleArray[toIndex + 1].frame.size.width
            }
            
            var visibleRect : CGRect = CGRectZero
            let windowWidth = buttonPreWidth + buttonCurWidth + buttonNexWidth
            if windowWidth < self.frame.size.width {
                visibleRect = CGRectMake(buttonPreX == -1 ? buttonCurX : buttonPreX, 0, windowWidth, self.segmentHeight+self.selectedSegmentIndiciterHeight)
            }
            else if fromIndex > toIndex {
                visibleRect = CGRectMake(buttonPreX == -1 ? buttonCurX : buttonPreX, 0, buttonPreWidth + buttonCurWidth, self.segmentHeight+self.selectedSegmentIndiciterHeight)
            }
            else {
                visibleRect = CGRectMake(buttonCurX, 0, buttonCurWidth + buttonNexWidth, self.segmentHeight+self.selectedSegmentIndiciterHeight)
            }
            
            dispatch_async(dispatch_get_main_queue(),{
                self.menuScrollView.scrollRectToVisible(visibleRect, animated: true)
                
                self.segmentTitleArray[fromIndex].selected = false
                self.segmentTitleArray[toIndex].selected = true
                
                UIView.animateWithDuration(0.3, animations: {
                    //moving highlited segment bar
                    
                    let button : UIButton = self.segmentTitleArray[toIndex]
                    
                    self.selectedSegmentIndiciter.frame = CGRectMake(button.frame.origin.x, self.segmentHeight, button.frame.size.width, self.selectedSegmentIndiciterHeight)
                    
                }) { (finished) in
                    if self.delegate != nil && self.delegate?.respondsToSelector(#selector(GSSegmentPagerDelegate.segmentPager(_:didMoveFromIndex:toIndex:))) == true {
                        self.delegate!.segmentPager!(self, didMoveFromIndex: fromIndex, toIndex: toIndex)
                    }
                }
            })
            
        })
        
        
    }
    
    
    func getWidthForString(string : String) -> CGFloat {
        let constraintRect = CGSize(width: CGFloat.max, height: self.segmentHeight)
        let boundingBox = string.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: segmentTitleFont], context: nil)
        return boundingBox.width + 20
    }
    
    func segmentButtonAction(sender : UIButton) {
        self.selectSegmentAtIndex(sender.tag, withAnimation: true)
    }
    
    public func selectSegmentAtIndex(index : NSInteger, withAnimation animation : Bool) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0),{
            print("Hello ---\(self.pageViewController.viewControllers)")
            if index >= self.segmentTitleArray.count {
                return
            }
            let fromIndex = self.currentSegmentIndex
            let toIndex = index
            if fromIndex == toIndex {
                return
            }
            let viewController = self.getViewControllerAtIndex(toIndex)
            if viewController == nil{
                return
            }
            let direction : UIPageViewControllerNavigationDirection = fromIndex < toIndex ? .Forward : .Reverse
            self.currentSegmentIndex = toIndex
            dispatch_async(dispatch_get_main_queue(),{
                self.pageViewController.setViewControllers( [viewController!], direction: direction, animated: animation) { (finished) in
                    self.moveHighlitedSegmentBarFromIndex(fromIndex, toIndex: toIndex)
                }
            })
        })
        
    }
    
}

//
//  GSLable.swift
//  Theme
//
//  Created by Vasim Akram on 8/29/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit
@objcMembers
@IBDesignable open class GSLable: UILabel {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override public func drawRect(rect: CGRect) {
        // Drawing code
     
    }
 */
    
    open var fontStyle : GS_FontStyle = .regular {
        didSet{
            self.font = GSFont.fontWithStyle(fontStyle, andSize: self.font.pointSize)
        }
    }
    
    open var styleSheet : GS_StyleSheet = .defaultStyle{
        didSet{
            self.font = GSStyleSheet.fontWithStyleSheet(styleSheet)
            self.textColor = GSStyleSheet.colorWithStyleSheet(styleSheet)
            //self.textAlignment = GSStyleSheet.textAlignmentWithStyleSheet(styleSheet)
        }
    }
    
    required public init(coder aDecoder: NSCoder) {
        super.init(coder:aDecoder)!
        self.configureLable()
    }
    
    override public init(frame: CGRect) {
        super.init(frame: frame)
        self.configureLable()
    }
    
    @IBInspectable open var colorOpacity : NSInteger = 100 {
        didSet{
            self.textColor = self.textColor.withAlphaComponent(CGFloat(colorOpacity) * 0.01)
        }
    }
    
    func configureLable() {
        //self.styleSheet = .defaultStyle
        self.fontStyle = .regular
    }
}

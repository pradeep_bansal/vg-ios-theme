//
//  LableButtonVC.swift
//  Theme
//
//  Created by Vasim Akram on 9/8/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit
import Theme

class LableButtonVC: UIViewController {

    @IBOutlet var aLable : GSLable!
    @IBOutlet var aSubLable : GSLable!
    @IBOutlet var aButton : GSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        aLable.text = "Hyundai introduces fuel efficient engine rrange"
        aLable.styleSheet = .titleBlack
        aSubLable.text = "Hyundai introduces fuel efficient engine range"
        aSubLable.styleSheet = .subheadingBlack
        
        aButton.setTitle("START NEW COMPARISON", for: .normal)
        //aButton.styleSheet = .heading
        aButton.buttonStyle = .filled
        //aButton.addTarget(self, action: #selector(LableButtonVC.buttonAction(sender:)), for: .touchUpInside)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonAction(sender : UIButton){
        sender.isEnabled = !sender.isEnabled;
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

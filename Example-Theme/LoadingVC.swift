//
//  LoadingVC.swift
//  Theme
//
//  Created by Vasim Akram on 9/8/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit
import Theme

class LoadingVC: UIViewController {

    @IBOutlet var aActivityIndicator : GSActivityIndicator!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        aActivityIndicator.startAnimating()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func buttonAction(_ sender : UIButton) {
        GSLoading.addLoadingWindow()
        
        let dispatchTime: DispatchTime = DispatchTime.now() + Double(Int64(2 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
        DispatchQueue.main.asyncAfter(deadline: dispatchTime, execute: {
            // your function here
            
            GSLoading.removeLoadingWindow()
        })
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

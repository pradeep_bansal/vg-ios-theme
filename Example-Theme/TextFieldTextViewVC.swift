//
//  TextFieldTextViewVC.swift
//  Theme
//
//  Created by Vasim Akram on 9/8/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit
import Theme


class TextFieldTextViewVC: UIViewController, GSTextFieldDelegate, GSTextFieldDataSource {

    @IBOutlet var txtEmail : GSTextField!
    @IBOutlet var txtPassword : GSTextField!
    @IBOutlet var aTextView : GSTextView!
    @IBOutlet var txtCity : GSTextField!
    
    @IBOutlet var txtsubCity : GSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        txtEmail.validationType = .email
        txtCity.textFieldType = .selection
        txtsubCity.textFieldType = .selection
        txtCity.minimumCharacterForSearch = 0
//        txtCity.showPopUpInMiddle = true
        //txtCity.dropDownArray = ["jaipur", "Alwar", "Jodhpur", "Ajmer", "Kota"]
        txtCity.delegate = self
        txtCity.dataSource = self
        //txtCity.hideBottomLine = true
        
        //txtEmail.text = ""
        
        txtsubCity.visibleRowCount = 2
        txtsubCity.delegate = self
        txtsubCity.dataSource = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func textField(_ textField: GSTextField, didSelectItemAtIndex index: NSInteger) {
        NSLog("selected index : %d",index)
    }
    
    
    func noOfRowsInDropdownTextField(_ textfield: GSTextField) -> NSInteger {
        return 5;
    }
    func textfield(_ textfield: GSTextField, dropdownTitleForRowAtIndex index: NSInteger) -> String {
        return ["jaipur", "Alwar", "Jodhpur", "Ajmer", "Kota"][index]
    }
    
    @IBAction func setTextButtonAction(button : UIButton) {
        if button.tag == 0 {
            txtEmail.text = "test@gmail.com"
        }
        else {
            txtEmail.text = ""
        }
    }
    
//    func textfield(_ textfield: GSTextField, dropdownSubTitleForRowAtIndex index: NSInteger) -> String {
//        return "Fri Apr 7, 2017"
//    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

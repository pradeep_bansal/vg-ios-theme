//
//  SlidersVC.swift
//  Theme
//
//  Created by Vasim Akram on 9/8/16.
//  Copyright © 2016 GirnarSoft. All rights reserved.
//

import UIKit
import Theme

class SlidersVC: UIViewController, GSRangeSliderDelegate {

    @IBOutlet var aSlider : GSRangeSlider!
    @IBOutlet var aLable : GSLable!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        aLable.numberOfLines = 0;
        aSlider.delegate = self;
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    // MARK: - RangeSlider Delegate
    func rangeSliderDidUpdateSliderLocation(_ slider: GSRangeSlider, lowerValue: Double, upperValue: Double) {
        aLable.text = "Lower value : \(lowerValue) \nUpper value : \(upperValue)"
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  SVGKImage+Color.h
//  SVGKit-iOS
//
//  Created by Vasim Akram on 1/25/17.
//  Copyright © 2017 na. All rights reserved.
//

#import "SVGKImage.h"

@interface SVGKImage (Color)

-(void)updateImageWithColor:(UIColor *)toColor;
-(void)updateImageColorHax:(NSString *)fromColor withColor:(UIColor *)toColor;
-(void)updateImageColor:(UIColor *)fromColor withColor:(UIColor *)toColor;

@end

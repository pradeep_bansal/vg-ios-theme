
Pod::Spec.new do |s|

# 1
s.platform = :ios
s.ios.deployment_target = '8.0'
s.name = "ThemeFramework"
s.summary = "A pod for ThemeFramework"
s.requires_arc = true

# 2
s.version = "0.0.4"

# 3
s.license = { :type => "MIT", :file => "LICENSE" }

# 4 - Replace with your name and e-mail address
s.author = { "Saurabh Goyal" => "saurabh.goyal@girnarsoft.com" }



# 5 - Replace this URL with your own Github page's URL (from the address bar)
s.homepage = "https://saurabhGoyalCD@bitbucket.org/vasim_akram/themeframework"



# 6 - Replace this URL with your own Git URL from "Quick Setup"
s.source = { :git => "https://saurabhGoyalCD@bitbucket.org/vasim_akram/themeframework.git", :tag => 'ThemeFramework-v'+String(s.version)}

# 7
s.framework = "UIKit"

# 8
s.vendored_frameworks = "ThemeFramework", "Theme.framework"
s.source_files = "ThemeFramework", "Theme/**/*.{h,m,swift}"

end